const tabTittle = document.querySelectorAll(".tabs-title");
const tabs = document.querySelector(".tabs");
const tabItem = document.querySelectorAll(".tab-item");

const cssToggle = (nodeArray, toggleCss , activeElement ) => {
    nodeArray.forEach((nodeItem) => {
        if (nodeItem.classList.contains(toggleCss)) {
            (nodeItem.classList.remove(toggleCss));
        }

    });
    activeElement.classList.add(toggleCss);
};

const toggleTabs = (nodeArray , elementId) => {
    nodeArray.forEach((nodeItem) => {
        if (nodeItem.id === elementId) {
            nodeItem.style.display = "block" ;
        }
        else {
            nodeItem.style.display = "none";
        }
    });
};

const tabsFunc = (event) => {
    const dataId = event.target.getAttribute("data-tab");
    const activeElement = document.getElementById(`tab-${dataId}`);
    cssToggle(tabTittle , 'active', event.target);
    toggleTabs(tabItem , activeElement.id);
} ;

tabs.addEventListener('click', (event) => {
    if (event.target.nodeName === "LI") {
        tabsFunc(event);
    }
});
